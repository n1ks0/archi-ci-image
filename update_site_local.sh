#!/bin/bash

set -euo pipefail

: "${ARCHI_PROJECT_PATH:=/archi/project}"
: "${ARCHI_REPORT_PATH:=/archi/report}"
: "${ARCHI_SITE_PATH:=/archi/site}"
: "${ARCHI_APP:=com.archimatetool.commandline.app}"
: "${REPORT_TYPE:=wip}"
: "${REPORT_VERSION:=v1}"
: "${REPORT_ADD_TIMESTAMP:=true}"
: "${INDEX_CURRENT_REPORT_VERSION:=""}"
: "${INDEX_WIP_REPORT_VERSION:=""}"

section_start() {
  printf '\e[0Ksection_start:%s:%s[collapsed=true]\r\e[0K\e[1;36m%s\e[0m\n' "$(date +%s)" "$1" "${*:2}"
}

section_end() {
  printf '\e[0Ksection_end:%s:%s\r\e[0K\n' "$(date +%s)" "$*"
}

fail() {
  printf >&2 '%s\n' "$*";
  exit 1;
}

cd "$ARCHI_PROJECT_PATH" && mkdir -p "$ARCHI_REPORT_PATH"

section_start 'report.build' 'Build ArchiMate report'

archi_args=()

archi_args=(
    --html.createReport
      "${ARCHI_REPORT_PATH}"
  )

xvfb-run /opt/Archi/Archi -application "$ARCHI_APP" -consoleLog -nosplash --modelrepository.loadModel "$ARCHI_PROJECT_PATH" "${archi_args[@]}" &&
printf '\n%s\n\n' "Report saved to $ARCHI_REPORT_PATH"

section_end 'report.build' 'ArchiMate report built'

section_start 'site.updating' 'Site files update'

echo "run index update script"
export REPORT_TYPE && export REPORT_VERSION && export REPORT_ADD_TIMESTAMP && python3 /opt/Archi/modify_index.py "$ARCHI_REPORT_PATH/index.html"

if [ "$REPORT_TYPE" == "wip" ]; then
  WIP_FILE_PATH=$(find "/site" -name .wip)
  if [ "$WIP_FILE_PATH" ]; then
    WIP_REPORT_VERSION=$(cat "$WIP_FILE_PATH")
    if [ "$WIP_REPORT_VERSION" == "$REPORT_VERSION" ]; then
      echo "update WIP version $WIP_REPORT_VERSION"
    else
      fail "WIP versions mismatch: commit - $REPORT_VERSION, site - $WIP_REPORT_VERSION"
    fi
  fi
fi

if [ "$REPORT_TYPE" == "current" ]; then
   CURRENT_FILE_PATH=$(find "$ARCHI_SITE_PATH" -name .current)
   if [ "$CURRENT_FILE_PATH" ]; then
     CURRENT_DIR_PATH=$(dirname "$CURRENT_FILE_PATH")
     echo "make $CURRENT_DIR_PATH outdated"
     CURRENT_REPORT_VERSION=$(cat "$CURRENT_FILE_PATH")
     REPORT_TYPE=outdated REPORT_VERSION=$CURRENT_REPORT_VERSION python3 /opt/Archi/modify_index.py "$CURRENT_DIR_PATH/index.html"
     mv "/$CURRENT_DIR_PATH/.current" "$CURRENT_DIR_PATH/.outdated"
   fi
fi

echo "copy report $REPORT_VERSION ($REPORT_TYPE) to site folder"
rm -rf "${ARCHI_SITE_PATH:?}/$REPORT_VERSION"
mkdir -p "$ARCHI_SITE_PATH/$REPORT_VERSION"
cp -r "$ARCHI_REPORT_PATH"/* "$ARCHI_SITE_PATH/$REPORT_VERSION/"
echo "$REPORT_VERSION" > "$ARCHI_SITE_PATH/$REPORT_VERSION/.$REPORT_TYPE"


echo "gathering information for index page"
WIP_FILE_PATH=$(find "/site" -name .wip)
if [ "$WIP_FILE_PATH" ]; then
  INDEX_WIP_REPORT_VERSION=$(cat "$WIP_FILE_PATH")
  echo "found WIP - $INDEX_WIP_REPORT_VERSION"
fi
CURRENT_FILE_PATH=$(find "/site" -name .current)
if [ "$CURRENT_FILE_PATH" ]; then
  INDEX_CURRENT_REPORT_VERSION=$(cat "$CURRENT_FILE_PATH")
  echo "found CURRENT - $INDEX_CURRENT_REPORT_VERSION"
fi
if [ "$INDEX_CURRENT_REPORT_VERSION" ]; then
  if [ "$INDEX_WIP_REPORT_VERSION" ]; then
    cp /opt/Archi/index.html.template /site/index.html
    sed -i "s/%CURRENT%/$INDEX_CURRENT_REPORT_VERSION/g" /site/index.html
    sed -i "s/%WIP%/$INDEX_WIP_REPORT_VERSION/g" /site/index.html
    echo "index.html created"
  else
    cp /opt/Archi/index.html.template /site/index.html
    sed -i "s/%CURRENT%/$INDEX_CURRENT_REPORT_VERSION/g" /site/index.html
    sed -i "s/%WIP%/$INDEX_CURRENT_REPORT_VERSION/g" /site/index.html
    echo "index.html created"
  fi
else
  if [ "$INDEX_WIP_REPORT_VERSION" ]; then
    cp /opt/Archi/index.html.template /site/index.html
    sed -i "s/%CURRENT%/$INDEX_WIP_REPORT_VERSION/g" /site/index.html
    sed -i "s/%WIP%/$INDEX_WIP_REPORT_VERSION/g" /site/index.html
    echo "index.html created"
  else
    echo "can't create index.html - neither WIP nor CURRENT exists"
  fi
fi

section_end 'site.updating' 'Site files updated'

exit 0
