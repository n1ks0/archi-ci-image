#!/bin/bash

set -euo pipefail

: "${ARCHI_REPORT_PATH:=/archi/report}"
: "${ARCHI_APP:=com.archimatetool.commandline.app}"
: "${REPORT_TYPE:=wip}"
: "${REPORT_VERSION:=v1}"
: "${REPORT_ADD_TIMESTAMP:=true}"
: "${REPORT_ADD_TIMESTAMP:=true}"
: "${SITE_REPO_COMMIT_TAG:=""}"
: "${SITE_REPO_COMMIT_BRANCH:=""}"
: "${SITE_REPO_COMMIT_BRANCH:=""}"
: "${INDEX_CURRENT_REPORT_VERSION:=""}"
: "${INDEX_WIP_REPORT_VERSION:=""}"

section_start() {
  printf '\e[0Ksection_start:%s:%s[collapsed=true]\r\e[0K\e[1;36m%s\e[0m\n' "$(date +%s)" "$1" "${*:2}"
}

section_end() {
  printf '\e[0Ksection_end:%s:%s\r\e[0K\n' "$(date +%s)" "$*"
}

fail() {
  printf >&2 '%s\n' "$*";
  exit 1;
}

[ -z "$ARCHI_PROJECT_PATH" ] && fail 'ARCHI_PROJECT_PATH must be set'
[ -z "$SITE_REPO_URL" ] && fail 'SITE_REPO_URL must be set'
[ -z "$SITE_REPO_BRANCH" ] && fail 'SITE_REPO_BRANCH must be set'
[ -z "$SITE_REPO_USER_NAME" ] && fail 'SITE_REPO_USER_NAME must be set'
[ -z "$SITE_REPO_USER_EMAIL" ] && fail 'SITE_REPO_USER_EMAIL must be set'
[ -z "$SITE_COMMIT_MESSAGE" ] && fail 'SITE_COMMIT_MESSAGE must be set'


if [ -n "$SITE_REPO_COMMIT_TAG" ]; then
  REPORT_VERSION=$SITE_REPO_COMMIT_TAG
  REPORT_TYPE=current
  echo "master-branch job for version $REPORT_VERSION"
else
  if [ -n "$SITE_REPO_COMMIT_BRANCH" ]; then
    [[ "$SITE_REPO_COMMIT_BRANCH" =~ ^wip_..*$ ]] || fail 'wrong branch name'
    # shellcheck disable=SC2001
    REPORT_VERSION=$(echo "$SITE_REPO_COMMIT_BRANCH" | sed 's/^wip_\(..*\)$/\1/')
    [ -z "$REPORT_VERSION" ] && fail 'wrong branch name'
    echo "WIP-branch job for version $REPORT_VERSION"
    REPORT_TYPE=wip
  else
    fail 'either SITE_REPO_COMMIT_BRANCH or SITE_REPO_COMMIT_TAG must be set'
  fi
fi

rm -rf /archi/site
mkdir /archi/site
cd "$ARCHI_PROJECT_PATH" && mkdir -p "$ARCHI_REPORT_PATH"

section_start 'report.build' 'Build ArchiMate report'

archi_args=()

archi_args=(
    --html.createReport
      "${ARCHI_REPORT_PATH}"
  )

xvfb-run /opt/Archi/Archi -application "$ARCHI_APP" -consoleLog -nosplash --modelrepository.loadModel "$ARCHI_PROJECT_PATH" "${archi_args[@]}" &&
printf '\n%s\n\n' "Report saved to $ARCHI_REPORT_PATH"

section_end 'report.build' 'ArchiMate report built'


section_start 'site.download' 'Get site files'

git clone --branch "$SITE_REPO_BRANCH"  --depth 1 "$SITE_REPO_URL" /archi/site

section_end 'site.download' ''


section_start 'site.updating' 'Site files update'

echo "run index update script"
export REPORT_TYPE && export REPORT_VERSION && export REPORT_ADD_TIMESTAMP && python3 /opt/Archi/modify_index.py "$ARCHI_REPORT_PATH/index.html"

if [ "$REPORT_TYPE" == "wip" ]; then
  WIP_FILE_PATH=$(find "/archi/site" -name .wip)
  if [ "$WIP_FILE_PATH" ]; then
    WIP_REPORT_VERSION=$(cat "$WIP_FILE_PATH")
    if [ "$WIP_REPORT_VERSION" == "$REPORT_VERSION" ]; then
      echo "update WIP version $WIP_REPORT_VERSION"
    else
      fail "WIP versions mismatch: commit - $REPORT_VERSION, site - $WIP_REPORT_VERSION"
    fi
  fi
fi

if [ "$REPORT_TYPE" == "current" ]; then
   CURRENT_FILE_PATH=$(find "/archi/site" -name .current)
   if [ "$CURRENT_FILE_PATH" ]; then
     CURRENT_DIR_PATH=$(dirname "$CURRENT_FILE_PATH")
     echo "make $CURRENT_DIR_PATH outdated"
     CURRENT_REPORT_VERSION=$(cat "$CURRENT_FILE_PATH")
     REPORT_TYPE=outdated REPORT_VERSION=$CURRENT_REPORT_VERSION python3 /opt/Archi/modify_index.py "$CURRENT_DIR_PATH/index.html"
    mv "/$CURRENT_DIR_PATH/.current" "$CURRENT_DIR_PATH/.outdated"
   fi
fi

echo "copy report $REPORT_VERSION ($REPORT_TYPE) to site folder"
rm -rf "/archi/site/$REPORT_VERSION"
mkdir -p "/archi/site/$REPORT_VERSION"
cp -r "$ARCHI_REPORT_PATH"/* "/archi/site/$REPORT_VERSION/"
echo "$REPORT_VERSION" > "/archi/site/$REPORT_VERSION/.$REPORT_TYPE"

echo "gathering information for index page"
WIP_FILE_PATH=$(find "/archi/site" -name .wip)
if [ "$WIP_FILE_PATH" ]; then
  INDEX_WIP_REPORT_VERSION=$(cat "$WIP_FILE_PATH")
  echo "found WIP - $INDEX_WIP_REPORT_VERSION"
fi
CURRENT_FILE_PATH=$(find "/archi/site" -name .current)
if [ "$CURRENT_FILE_PATH" ]; then
  INDEX_CURRENT_REPORT_VERSION=$(cat "$CURRENT_FILE_PATH")
  echo "found CURRENT - $INDEX_CURRENT_REPORT_VERSION"
fi
if [ "$INDEX_CURRENT_REPORT_VERSION" ]; then
  if [ "$INDEX_WIP_REPORT_VERSION" ]; then
    cp /opt/Archi/index.html.template /archi/site/index.html
    sed -i "s/%CURRENT%/$INDEX_CURRENT_REPORT_VERSION/g" /archi/site/index.html
    sed -i "s/%WIP%/$INDEX_WIP_REPORT_VERSION/g" /archi/site/index.html
    echo "index.html created"
  else
    cp /opt/Archi/index.html.template /archi/site/index.html
    sed -i "s/%CURRENT%/$INDEX_CURRENT_REPORT_VERSION/g" /archi/site/index.html
    sed -i "s/%WIP%/$INDEX_CURRENT_REPORT_VERSION/g" /archi/site/index.html
    echo "index.html created"
  fi
else
  if [ "$INDEX_WIP_REPORT_VERSION" ]; then
    cp /opt/Archi/index.html.template /archi/site/index.html
    sed -i "s/%CURRENT%/$INDEX_WIP_REPORT_VERSION/g" /archi/site/index.html
    sed -i "s/%WIP%/$INDEX_WIP_REPORT_VERSION/g" /archi/site/index.html
    echo "index.html created"
  else
    echo "can't create index.html - neither WIP nor CURRENT exists"
  fi
fi

section_end 'site.updating' 'Site files updated'


section_start 'site.uploading' 'Commit site files'

cd /archi/site
git config user.name "$SITE_REPO_USER_NAME"
git config user.email "$SITE_REPO_USER_EMAIL"
# shellcheck disable=SC2035
git add *
git commit -m "$SITE_COMMIT_MESSAGE"
git push "$SITE_REPO_URL"
rm -rf .git/

section_end 'site.uploading' ''

exit 0
