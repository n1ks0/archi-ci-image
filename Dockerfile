FROM docker.io/ubuntu:22.04

ARG ARCHI_VERSION=5.0.2
ARG COARCHI_VERSION=0.8.8
ARG TZ=UTC
ARG UID=1000

WORKDIR /archi

SHELL ["/bin/bash", "-o", "pipefail", "-x", "-e", "-u", "-c"]

# DL3015 ignored for suppress org.freedesktop.DBus.Error.ServiceUnknown
# hadolint ignore=DL3008,DL3015
RUN ln -snf "/usr/share/zoneinfo/$TZ" /etc/localtime && \
    echo "$TZ" > /etc/timezone && \
    # Install dependecies \
    apt-get update && \
    apt-get install -y \
      ca-certificates \
      libgtk2.0-cil \
      libswt-gtk-4-jni \
      dbus-x11 \
      xvfb \
      curl \
      python3 \
      python3-bs4 \
      zip \
      unzip \
      git && \
    apt-get clean && \
    update-ca-certificates && \
    rm -rf /var/lib/apt/lists/*

RUN groupadd --gid "$UID" archi && \
    useradd --uid "$UID" --gid archi --shell /bin/bash --home-dir /archi --create-home archi && \
    # Download & extract Archimate tool \
    curl "https://www.archimatetool.com/downloads/archi.php?/$ARCHI_VERSION/Archi-Linux64-$ARCHI_VERSION.tgz" \
      --output - | \
      tar zxf - -C /opt/ 2> /dev/null && \
    chmod +x /opt/Archi/Archi && \
    # Install Collaboration plugin \
    mkdir -p /archi/.archi/dropins /archi/report /archi/project && \
    curl "https://www.archimatetool.com/downloads/coarchi/coArchi_$COARCHI_VERSION.archiplugin" \
       --output modelrepository.archiplugin && \
    unzip modelrepository.archiplugin -d /archi/.archi/dropins/ && \
    rm modelrepository.archiplugin && \
    chown -R "$UID:0" /archi && \
    chmod -R g+rw /archi


COPY modify_index.py index.html.template update_site_git.sh update_site_local.sh  /opt/Archi/

RUN chmod +x /opt/Archi/update_site_*.sh

USER archi
