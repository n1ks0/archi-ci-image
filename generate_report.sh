#!/bin/bash

set -euo pipefail

: "${ARCHI_PROJECT_PATH:=/archi/project}"
: "${ARCHI_REPORT_PATH:=/archi/report}"
: "${ARCHI_SITE_PATH:=/archi/site}"
: "${ARCHI_APP:=com.archimatetool.commandline.app}"
: "${REPORT_TYPE:=wip}"
: "${REPORT_VERSION:=v1}"
: "${REPORT_ADD_TIMESTAMP:=true}"

section_start() {
  printf '\e[0Ksection_start:%s:%s[collapsed=true]\r\e[0K\e[1;36m%s\e[0m\n' "$(date +%s)" "$1" "${*:2}"
}

section_end() {
  printf '\e[0Ksection_end:%s:%s\r\e[0K\n' "$(date +%s)" "$*"
}

fail() {
  printf >&2 '%s\n' "$*";
  exit 1;
}

cd "$ARCHI_PROJECT_PATH" && mkdir -p "$ARCHI_REPORT_PATH"

section_start 'report.build' 'Build ArchiMate report'

archi_args=()

archi_args=(
    --html.createReport
      "${ARCHI_REPORT_PATH}"
  )

xvfb-run /opt/Archi/Archi -application "$ARCHI_APP" -consoleLog -nosplash --modelrepository.loadModel "$ARCHI_PROJECT_PATH" "${archi_args[@]}" &&
printf '\n%s\n\n' "Report saved to $ARCHI_REPORT_PATH"

section_end 'report.build' 'ArchiMate report built'

section_start 'report.updating' 'Report update'

echo "run index update script"
export REPORT_TYPE && export REPORT_VERSION && export REPORT_ADD_TIMESTAMP && python3 /opt/Archi/modify_index.py "$ARCHI_REPORT_PATH/index.html"

section_end 'report.updating' 'Report updated'

exit 0
