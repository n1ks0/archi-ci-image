import os
import sys
import copy
from datetime import datetime

from bs4 import BeautifulSoup

TITLE_SPAN_ID = "generated_report_title"
GENERATED_TIMESTAMP_SPAN_ID = "generated_report_timestamp"

indexFilePath = sys.argv[1]

reportType = os.environ.get("REPORT_TYPE", "current")
reportVersionName = os.environ.get("REPORT_VERSION", "no version")
reportAddTimestamp = os.environ.get("REPORT_ADD_TIMESTAMP", "false").lower() == "true"

reportTimestamp = datetime.now().strftime("%d.%m.%Y %H:%M:%S")
if reportType == "wip":
    reportTypeString = "WORK IN PROGRESS"
elif reportType == "outdated":
    reportTypeString = "OUTDATED"
else:
    reportTypeString = "CURRENT"

with open(indexFilePath, "r", encoding="utf-8") as indexFile:
    indexHtml = BeautifulSoup(indexFile, "html.parser")

originalTitleElement = indexHtml.find_all("span", {"class": "navbar-brand", "id": None})[0]
titleElements = indexHtml.find_all("span", {"class": "navbar-brand", "id": TITLE_SPAN_ID})
if len(titleElements) == 0:
    titleElement = copy.copy(originalTitleElement)
    originalTitleElement['style'] = "display: none;"
    titleElement["id"] = TITLE_SPAN_ID
    originalTitleElement.parent.append(titleElement)
else:
    titleElement = titleElements[0]

titleString = "[" + reportTypeString + "] " + originalTitleElement.text + " (" + reportVersionName + ")"
titleElement.string.replace_with(titleString)

headerElement = indexHtml.find_all("nav", {"class": "navbar-inverse"})[0]

if reportType == "wip":
    headerElement["style"] = "background-color: red;"
elif reportType == "outdated":
    headerElement["style"] = "background-color: gray;"
else:
    headerElement["style"] = ""

if reportAddTimestamp:
    if len(indexHtml.find_all("span", {"id": GENERATED_TIMESTAMP_SPAN_ID})) == 0:
        generatedTimestampElement = copy.copy(originalTitleElement)
        generatedTimestampElement["id"] = GENERATED_TIMESTAMP_SPAN_ID
        generatedTimestampElement.string.replace_with("generated at " + reportTimestamp)
        generatedTimestampElement["style"] = ""
        originalTitleElement.parent.append(generatedTimestampElement)

with open(indexFilePath, "w", encoding="utf-8") as outputFile:
    outputFile.write(str(indexHtml))
